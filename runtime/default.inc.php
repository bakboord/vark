<?php

/*
Vark - compiles vark programs to PHP
Copyright (C) 2013 Alex Deleyn <alex@bakboord.be>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace
{
	function system_exit( $status )
	{
		exit( $status );
	}
	
	function array_remove( &$array, $key )
	{
		unset( $array[ $key ] );
	}
}

namespace vark\r
{
	// interpolation hack
	function i( $x ) { return $x; }
	
	// assert fix
	class AssertError extends \Exception
	{
	}
	
	// no catch clause hack
	class Never extends \Exception {}
	
	// finally hack
	class Finally_
	{
		public static $callbacks = array();
		
		public function __construct( $callback )
		{
			self::$callbacks[] = $callback;
		}
		
		public function __destruct()
		{
			call_user_func( array_pop( self::$callbacks ) );
		}
	}
	
	register_shutdown_function( function()
	{
		while( $f = array_pop( Finally_::$callbacks ) )
		{
			$f();
		}
	} );
}
