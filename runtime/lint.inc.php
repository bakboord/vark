<?php

namespace vark\r\lint;

class Error extends \Exception
{
}

function check_c1( $a, $b )
{
	if( is_string( $a ) ) throw new Error( 'left side of comparison is a string' );
	if( is_string( $b ) ) throw new Error( 'right side of comparison is a string' );
}
// @TODO check for float

function lt( $a, $b ) { check_c1( $a, $b ); return $a < $b; }
function gt( $a, $b ) { check_c1( $a, $b ); return $a > $b; }
function lte( $a, $b ) { check_c1( $a, $b ); return $a <= $b; }
function gte( $a, $b ) { check_c1( $a, $b ); return $a >= $b; }
function eq( $a, $b ) { return $a === $b; }
function neq( $a, $b ) { return $a !== $b; }
