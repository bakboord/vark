<?php

/*
Vark - compiles vark programs to PHP
Copyright (C) 2013 Alex Deleyn <alex@bakboord.be>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace vark\r;

abstract class DevAutoCompile
{
	public static function is_up_to_date( $file )
	{
		return filemtime( $file ) > filemtime( substr( $file, 0, -4 ) . '.vark' );
	}
	
	public static function recompile( $file )
	{
		$p = proc_open( __DIR__ . '/../vark ' . escapeshellarg( substr( $file, 0, -4 ) . '.vark' ),
			array(
				0 => array( 'pipe', 'r' ),
				1 => array( 'pipe', 'w' ),
				2 => array( 'pipe', 'w' ),
			),
			$pipes
		);
		
		$errors = stream_get_contents( $pipes[2] );
		$r = proc_close( $p );
		
		if( $r !== 0 )
		{
			echo '<pre>' . $errors . '</pre>';
			die();
		}
	}
}
