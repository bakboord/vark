var $c
var $b = function( $x )
{
	$c += 1
	return $x
}

$c = 0
assert $b( TRUE ) and $b( TRUE )
assert $c == 2

$c = 0
assert $b( TRUE ) and $b( TRUE ) and $b( TRUE )
assert $c == 3

$c = 0
assert $b( TRUE ) or $b( TRUE )
assert $c == 1

$c = 0
assert $b( TRUE ) or $b( TRUE ) or $b( TRUE )
assert $c == 1

$c = 0
assert !( $b( FALSE ) and $b( TRUE ) )
assert $c == 1

$c = 0
assert !( $b( TRUE ) and $b( FALSE ) and $b( TRUE ) )
assert $c == 2

$c = 0
assert $b( FALSE ) or $b( TRUE )
assert $c == 2

$c = 0
assert $b( FALSE ) or $b( TRUE ) or $b( TRUE )
assert $c == 2
