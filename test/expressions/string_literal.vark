assert strlen( "a" ) == 1

assert strlen( "\n" ) == 1
assert ord( "\n" ) == 10

assert strlen( "\t" ) == 1
assert ord( "\t" ) == 9

assert strlen( "$a" ) == 2
