class MyClass
{
	public static $foo = 4
	public $bar = 3
}

var $i = new MyClass()

assert `"MyClass"`::$foo == 4
assert `"MyClass"`::$`"foo"` == 4
assert $i::$`"foo"` == 4
assert $i.`"bar"` == 3
