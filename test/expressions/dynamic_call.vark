class MyClass
{
	public static foo()
	{
		return 3
	}
	
	public bar()
	{
		return 4
	}
}

function baz()
{
	return 5
}

var $i = new MyClass()

assert MyClass::`"foo"`() == 3
assert `"MyClass"`::foo() == 3
assert $i::`"foo"`() == 3
assert $i.`"bar"`() == 4
assert `"baz"`() == 5
