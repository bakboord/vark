namespace test

const CONSTANT = 1

class MyClass
{
	const CONSTANT = 2
	public static $static_property = 3
	public $instance_property = 4
}

var $a = new MyClass()

assert CONSTANT == 1
assert php.test.CONSTANT == 1

assert MyClass.CONSTANT == 2
assert php.test.MyClass.CONSTANT == 2

assert MyClass.$static_property == 3
assert php.test.MyClass.$static_property == 3

assert $a::$static_property == 3
assert $a.$instance_property == 4
