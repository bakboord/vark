var $c
var $f = function( $x )
{
	return $x * $c
}

$c = 2
assert $f( 1 ) == 2
assert $f( 2 ) == 4

$c = 3
assert $f( 1 ) == 3
assert $f( 2 ) == 6
