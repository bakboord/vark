var $a
var $f = lambda $x: $a = $x

$f( 1 )
assert $a == 1

$f( 3 )
assert $a == 3
