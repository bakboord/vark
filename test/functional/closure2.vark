var $a = 3
var $f = function( $x )
{
	$a = $x
}

$f( 1 )
assert $a == 1

$f( 3 )
assert $a == 3
