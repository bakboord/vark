class Foo
{
}

interface Bar
{
}

interface Baz
{
}

class Qux extends Foo implements Bar, Baz
{
}

var $q = new Qux()
assert $q instanceof Foo
assert $q instanceof Bar
assert $q instanceof Baz
assert $q instanceof Qux
