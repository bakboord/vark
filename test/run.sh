#!/bin/bash

c=0
C=0
e=0
E=0

test() {
	echo "[ $1 ]"
	echo "  compiling..."
	C=`expr $C + 1`
	vark $1 2>&1 | sed "s/^/    /"
	if [ ${PIPESTATUS[0]} -eq 0 ]
	then
		c=`expr $c + 1`
		echo "  executing..."
		E=`expr $E + 1`
		php -d assert.bail=1 "${1:0:-5}.php" | sed "s/^/    /"
		if [ ${PIPESTATUS[0]} -eq 0 ]
		then
			e=`expr $e + 1`
		fi
	else
		echo "	(skipping execution of $1)"
	fi
	echo
}

if [ $# -eq 0 ]
then
	cd `dirname $0`
	
	for f in */*.vark
	do
		test $f
	done
else
	for f in $@
	do
		test $f
	done
fi

echo "RESULTS:"
echo "compilation ....... $c/$C"
echo "execution ......... $e/$E"
