<?php

/*
Vark - compiles vark programs to PHP
Copyright (C) 2013 Alex Deleyn <alex@bakboord.be>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace vark\c\ast\util;
use vark\c\parse;
use vark\c\ast;

function parse_block( $source, $trailing_newline = TRUE )
{
	$nodes = array();
	$nodes[] = $source->read( '{' );
	$nodes[] = $source->read( 'newline' );
	while( $source->peek()->type !== '}' )
	{
		$nodes[] = parse_statement( $source );
	}
	$nodes[] = $source->read( '}' );
	
	if( $trailing_newline )
	{
		$nodes[] = $source->read( 'newline' );
	}
	
	return new parse\Node( $nodes );
}

function parse_statement( $source )
{
	switch( $source->peek()->type )
	{
		case 'abstract':
		case 'class':
			return ast\Class_::parse( $source );
		case 'interface':
			return ast\Interface_::parse( $source );
		case 'function':
			return ast\Function_::parse( $source );
		case 'namespace':
			return ast\Namespace_::parse( $source );
		case 'use':
			return ast\Use_::parse( $source );
		case 'while':
			return ast\While_::parse( $source );
		case 'foreach':
			return ast\ForEach_::parse( $source );
		case 'for':
			return ast\For_::parse( $source );
		case 'if':
			return ast\If_::parse( $source );
		case 'try':
			return ast\Try_::parse( $source );
		case 'throw':
			return ast\Throw_::parse( $source );
		case 'break':
			return ast\Break_::parse( $source );
		case 'continue':
			return ast\Continue_::parse( $source );
		case 'return':
			return ast\Return_::parse( $source );
		case 'newline':
			return ast\Nop::parse( $source );
		case 'var':
			return ast\Var_::parse( $source );
		case 'const':
			return ast\Constant::parse( $source );
		case 'print':
			return ast\Print_::parse( $source );
		case 'assert':
			return ast\Assert::parse( $source );
		case 'runtime_lint_enable':
		case 'runtime_lint_disable':
			return ast\RuntimeLint::parse( $source );
		default:
			return ast\ExpressionStatement::parse( $source );
	}
}

const PRECEDENCE_INCLUDE = 10;
const PRECEDENCE_IN = 9;
const PRECEDENCE_TYPE = 8; // instanceof and cast
const PRECEDENCE_NOT = 7;
const PRECEDENCE_MULTIPLICATIVE = 6;
const PRECEDENCE_ADDITIVE = 5;
const PRECEDENCE_COMPARISON = 4;
const PRECEDENCE_TERNARY = 3;
const PRECEDENCE_ASSIGNMENT = 2; // assignment or alias
const PRECEDENCE_LOGICAL = 1; // and/or
const PRECEDENCE_ANY = 0;

function parse_expression( $source, $min_precedence = PRECEDENCE_ANY )
{
	if( $min_precedence <= PRECEDENCE_NOT && $source->peek( '!' ) )
	{
		$nodes = array();
		$nodes[] = $source->read();
		$nodes[] = parse_expression( $source, PRECEDENCE_TYPE );
		$left = new ast\Not( $nodes );
	}
	elseif( $min_precedence <= PRECEDENCE_INCLUDE && $source->peek_any( array( 'include', 'include_once', 'require', 'require_once' ) ) )
	{
		$nodes = array();
		$nodes[] = $source->read();
		$nodes[] = $source->read( '(' );
		$nodes[] = parse_expression( $source, PRECEDENCE_ANY );
		$nodes[] = $source->read( ')' );
		$left = new ast\Include_( $nodes );
	}
	else
	{
		$left = parse_access( $source );
	}
	
	if( $source->peek( 'in' ) )
	{
		$nodes = array( $left );
		$nodes[] = $source->read();
		$nodes[] = parse_expression( $source, PRECEDENCE_IN + 1 );
		$left = new ast\In( $nodes );
	}
	
	if( $min_precedence > PRECEDENCE_TYPE ) goto end;
	
	if( $source->peek( 'instanceof' ) )
	{
		$nodes = array( $left );
		$nodes[] = $source->read();
		$nodes[] = parse_type( $source );
		$left = new ast\InstanceOf_( $nodes );
	}
	/* @TODO cast syntax else if( $source->peek( 'as' ) )
	{
		$nodes = array( $left );
		$nodes[] = $source->read();
		$nodes[] = parse_type( $source );
		$left = new ast\Cast_( $nodes );
	}*/
	
	if( $min_precedence > PRECEDENCE_MULTIPLICATIVE ) goto end;
	
	while( $source->peek_any( array( '*', '/', '%' ) ) )
	{
		$nodes = array( $left );
		$nodes[] = $source->read();
		$nodes[] = parse_expression( $source, PRECEDENCE_MULTIPLICATIVE + 1 );
		$left = new ast\Multiplicative( $nodes );
	}
	
	if( $min_precedence > PRECEDENCE_ADDITIVE ) goto end;
	
	while( $source->peek_any( array( '+', '-', '...' ) ) )
	{
		$nodes = array( $left );
		$nodes[] = $source->read();
		$nodes[] = parse_expression( $source, PRECEDENCE_ADDITIVE + 1 );
		$left = new ast\Additive( $nodes );
	}
	
	if( $min_precedence > PRECEDENCE_COMPARISON ) goto end;
	
	if( $source->peek_any( array( '==', '!=', '<', '>', '<=', '>=' ) ) )
	{
		$nodes = array( $left );
		$nodes[] = $source->read();
		$nodes[] = parse_expression( $source, PRECEDENCE_COMPARISON + 1 );
		$left = new ast\Comparison( $nodes );
	}
	
	if( $min_precedence > PRECEDENCE_TERNARY ) goto end;
	
	if( $source->peek( '?' ) )
	{
		$nodes = array( $left );
		$nodes[] = $source->read();
		$nodes[] = ( $source->peek( ':' ) ? NULL : parse_expression( $source, PRECEDENCE_TERNARY ) );
		$nodes[] = $source->read( ':' );
		$nodes[] = parse_expression( $source, PRECEDENCE_TERNARY );
		$left = new ast\Ternary( $nodes );
	}
	
	if( $min_precedence > PRECEDENCE_ASSIGNMENT ) goto end;
	
	// @TODO lvalue checks
	if( $source->peek_any( array( '=', '+=', '-=', '*=', '/=', '...=' ) ) )
	{
		$nodes = array( $left );
		$nodes[] = $source->read();
		$nodes[] = parse_expression( $source, PRECEDENCE_ASSIGNMENT );
		$left = new ast\Assignment( $nodes );
	}
	else if( $source->peek( '=&' ) )
	{
		$nodes = array( $left );
		$nodes[] = $source->read();
		$nodes[] = parse_expression( $source, PRECEDENCE_ASSIGNMENT );
		$left = new ast\Assignment( $nodes );
	}
	
	if( $min_precedence > PRECEDENCE_LOGICAL ) goto end;
	
	if( $source->peek( 'or' ) )
	{
		do
		{
			$nodes = array( $left );
			$nodes[] = $source->read();
			$nodes[] = parse_expression( $source, PRECEDENCE_LOGICAL + 1 );
			$left = new ast\Logical( $nodes );
		}
		while( $source->peek( 'or' ) );
	}
	else
	{
		while( $source->peek( 'and' ) )
		{
			$nodes = array( $left );
			$nodes[] = $source->read();
			$nodes[] = parse_expression( $source, PRECEDENCE_LOGICAL + 1 );
			$left = new ast\Logical( $nodes );
		}
	}
	
	end:
	return $left;
}

function parse_arguments( $source )
{
	if( $source->peek()->type === ')' )
	{
		return array();
	}
	else
	{
		$arguments = array();
		
		for(;;)
		{
			$arguments[] = ast\Argument::parse( $source );
			
			if( $source->peek()->type === ')' )
			{
				return $arguments;
			}
			else
			{
				$arguments[] = $source->read( ',' );
			}
		}
		
		assert( FALSE );
	}
}

function parse_call_arguments( $source )
{
	if( $source->peek( ')' ) )
	{
		return array();
	}
	else
	{
		$arguments = array();
		
		for(;;)
		{
			if( $source->peek()->type === '&' )
			{
				$t = $source->read();
				$t->value = '/*&*/'; // @TODO this is just a temporary hack
				$arguments[] = $t;
			}
			
			$arguments[] = parse_expression( $source );
			
			if( $source->peek()->type === ')' )
			{
				return $arguments;
			}
			else
			{
				$arguments[] = $source->read( ',' );
			}
		}
	}
	
	return $arguments;
}

function parse_lvalue( $source )
{
	return parse_access( $source );
}

function parse_access( $source )
{
	$left = parse_atom( $source );
	
	for(;;)
	{
		if( $source->peek( '[' ) )
		{
			$nodes = array( $left );
			$nodes[] = $source->read();
			$nodes[] = ( $source->peek( ']' ) ? NULL : parse_expression( $source ) );
			$nodes[] = $source->read( ']' );
			$left = new ast\ArrayAccess( $nodes );
		}
		else if( $source->peek( '(' ) )
		{
			$nodes = array( $left );
			$nodes[] = $source->read();
			if( $source->peek()->type === '*' )
			{
				$nodes[] = $source->read();
				$nodes[] = parse_expression( $source );
				$nodes[] = $source->read( ')' );
				$left = new ast\StarCall( $nodes );
			}
			else
			{
				$nodes = array_merge( $nodes, parse_call_arguments( $source ) );
				$nodes[] = $source->read( ')' );
				$left = new ast\Call( $nodes );
			}
		}
		else if( $source->peek( '.' ) )
		{
			$nodes = array( $left );
			$nodes[] = $source->read();
			if( $source->peek( '$' ) )
			{
				$nodes[] = $source->read();
				$nodes[] = ( $source->peek( '`' ) ? ast\Interpolation::parse( $source ) : $source->read( 'identifier' ) );
				$left = new ast\PropertyAccess( $nodes );
			}
			else
			{
				$nodes[] = ( $source->peek( '`' ) ? ast\Interpolation::parse( $source ) : $source->read( 'identifier' ) );
				$left = new ast\NameAccess( $nodes );
			}
		}
		else if( $source->peek( '::' ) )
		{
			$nodes = array( $left );
			$nodes[] = $source->read();
			if( $source->peek( '$' ) )
			{
				$nodes[] = $source->read();
				$nodes[] = ( $source->peek( '`' ) ? ast\Interpolation::parse( $source ) : $source->read( 'identifier' ) );
				$left = new ast\StaticPropertyAccess( $nodes );
			}
			else
			{
				$nodes[] = ( $source->peek( '`' ) ? ast\Interpolation::parse( $source ) : $source->read( 'identifier' ) );
				$left = new ast\StaticNameAccess( $nodes );
			}
		}
		else
		{
			return $left;
		}
	}
	
	assert( FALSE );
}

function parse_atom( $source )
{
	$t = $source->peek();
	
	switch( $t->type )
	{
		case '-':
			return new ast\NumberLiteral( array( $source->read(), $source->read( 'number' ) ) );
		case 'number':
			return new ast\NumberLiteral( array( $source->read() ) );
		case 'string':
			return new ast\StringLiteral( array( $source->read() ) );
		case '$':
			return ast\Variable::parse( $source );
		case '[':
			return ast\ArrayLiteral::parse( $source );
		case 'function':
			return ast\Closure::parse( $source );
		case 'lambda':
			return ast\Lambda::parse( $source );
		case 'new':
			return ast\New_::parse( $source );
		case '(':
			return ast\Parenthesized::parse( $source );
		case '`':
			return ast\Interpolation::parse( $source );
		case 'TRUE':
		case 'FALSE':
			return ast\BooleanLiteral::parse( $source );
		case 'NULL':
			return ast\NullLiteral::parse( $source );
		default:
			if( $t->super_type === 'identifier' )
			{
				return new ast\Identifier( array( $source->read() ) );
			}
			else
			{
				throw new \vark\c\parse\Error( 'expected expression, got ' . $t->type, $t );
			}
	}
}

function parse_type( $source )
{
	return ast\Name::parse( $source );
}
