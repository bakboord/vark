<?php

/*
Vark - compiles vark programs to PHP
Copyright (C) 2013 Alex Deleyn <alex@bakboord.be>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace vark\c\ast;
use vark\c\parse;

class Assignment extends parse\Node
{
	public function evaluate( $scope )
	{
		assert( $this[1]->value === '=' ); // @TODO
		$l =& $this[0]->evaluate_lvalue( $scope );
		$r = $this[2]->evaluate( $scope );
		$l = $r;
	}
	
	public function phpify( $out )
	{
		$out( $this[0] );
		if( $this[1]->value === '...=' )
		{
			$out( $this[1]->whitespace, '.=' );
		}
		else
		{
			$out( $this[1] );
		}
		$out( $this[2] );
	}
}
