<?php

/*
Vark - compiles vark programs to PHP
Copyright (C) 2013 Alex Deleyn <alex@bakboord.be>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace vark\c\ast;
use vark\c\parse;

class Class_ extends parse\Node
{
	public static function parse( $source )
	{
		$nodes = array();
		
		$keywords = array();
		while( !$source->peek( 'class' ) )
		{
			$keywords[] = $source->read_any( array( 'abstract', 'final' ) );
		}
		$nodes[] = new parse\Node( $keywords );
		
		$nodes[] = $source->read( 'class' );
		$nodes[] = $source->read( 'identifier' );
		
		$extends = array();
		if( $source->peek( 'extends' ) )
		{
			$extends[] = $source->read();
			$extends[] = util\parse_type( $source );
		}
		$nodes[] = new parse\Node( $extends );
		
		$implements = array();
		if( $source->peek( 'implements' ) )
		{
			$implements[] = $source->read();
			
			for(;;)
			{
				$implements[] = util\parse_type( $source );
				
				if( $source->peek()->type === ',' )
				{
					$implements[] = $source->read();
				}
				else
				{
					break;
				}
			}
		}
		$nodes[] = new parse\Node( $implements );
		
		$nodes[] = $source->read( 'newline' );
		
		$block = array();
		$block[] = $source->read( '{' );
		$block[] = $source->read( 'newline' );
		for(;;)
		{
			if( $source->peek( '}' ) )
			{
				break;
			}
			else if( $source->peek( 'const' ) )
			{
				$block[] = ClassConstant::parse( $source );
			}
			else
			{
				for( $d = 1 ; ; $d += 1 )
				{
					$t = $source->peek_ahead( $d );
					
					switch( $t->type )
					{
						case 'public':
						case 'protected':
						case 'private':
						case 'static':
						case 'identifier':
							continue 2;
						case '$':
							$block[] = Property::parse( $source );
							break 2;
						case '(':
						case 'abstract':
						case 'final':
						case '&':
							$block[] = Method::parse( $source );
							break 2;
						default:
							throw new parse\Error( 'unexpected ' . $t->type, $t );
					}
				}
			}
		}
		$block[] = $source->read( '}' );
		$block[] = $source->read( 'newline' );
		$nodes[] = new parse\Node( $block );
		
		return new self( $nodes );
	}
}
