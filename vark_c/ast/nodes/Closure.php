<?php

/*
Vark - compiles vark programs to PHP
Copyright (C) 2013 Alex Deleyn <alex@bakboord.be>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace vark\c\ast;
use vark\c\parse;

class Closure extends parse\Node
{
	private $scope;
	
	public static function parse( $source )
	{
		$nodes = array();
		
		$nodes[] = $source->read( 'function' );
		$nodes[] = ( $source->peek( '&' ) ? $source->read() : NULL );
		$nodes[] = ( !$source->peek( '(' ) ? util\parse_type( $source ) : NULL );
		$nodes[] = $source->read( '(' );
		$nodes[] = new parse\Node( util\parse_arguments( $source ) );
		$nodes[] = $source->read( ')' );
		$nodes[] = ( $source->peek( 'newline' ) ? $source->read() : NULL );
		$nodes[] = util\parse_block( $source, FALSE );
		return new self( $nodes );
	}
	
	public function analyze_locals( $scope )
	{
		$this->scope = $scope->spawn_closure();
		foreach( $this[4] as $i => $a )
		{
			if( $i % 2 === 0 )
			{
				$this->scope->declare_( $a[2], $a[3]->value );
			}
		}
		$this[-1]->analyze_locals( $this->scope );
	}
	
	public function phpify( $out )
	{
		$inherited = $this->scope->inherited;
		$this_hack = in_array( 'this', $inherited );
		
		if( $this_hack )
		{
			$out( 'i(' );
			$this_tmp = $this->scope->create_tmp_var();
			$inherited[ array_search( 'this', $inherited ) ] = $this_tmp;
		}
		
		$out(
			$this[0],
			$this[1]
		);
		$out->comment( $this[2] );
		$out( $this->slice( 3, 6 ) );
		
		if( $this->scope->inherited )
		{
			$out(
				'use(',
				implode( ', ',
					array_map( function( $v ) { return '&$' . $v; }, $inherited )
				),
				')'
			);
		}
		
		$out(
			$this[-2],
			$this[-1]->slice( 0, 2 )
		);
		
		if( $this_hack )
		{
			$out( '${\'this\'.\'\'}=$' . $this_tmp . ';' );
		}
		
		$out(
			$this[-1]->slice( 2 )
		);
		
		if( $this_hack )
		{
			$out( ',$' . $this_tmp . '=$this)' );
		}
	}
}
