<?php

/*
Vark - compiles vark programs to PHP
Copyright (C) 2013 Alex Deleyn <alex@bakboord.be>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace vark\c\ast;
use vark\c\parse;

class ForEach_ extends parse\Node
{
	public static function parse( $source )
	{
		$nodes = array();
		$nodes[] = $source->read( 'foreach' );
		$nodes[] = util\parse_expression( $source );
		$nodes[] = $source->read( 'as' );
		$nodes[] = ( $source->peek( 'var' ) ? $source->read() : NULL );
		$nodes[] = ( $source->peek( '&' ) ? $source->read() : NULL );
		$nodes[] = $source->read( '$' );
		$nodes[] = $source->read( 'identifier' );
		if( $source->peek( ':' ) )
		{
			$nodes[] = $source->read();
			$nodes[] = ( $source->peek( 'var' ) ? $source->read() : NULL );
			$nodes[] = ( $source->peek( '&' ) ? $source->read() : NULL );
			$nodes[] = $source->read( '$' );
			$nodes[] = $source->read( 'identifier' );
		}
		$nodes[] = $source->read( 'newline' );
		$nodes[] = util\parse_block( $source );
		return new self( $nodes );
	}
	
	public function analyze_locals( $scope )
	{
		$this[1]->analyze_locals( $scope );
		
		if( $this[3] )
		{
			$scope->declare_( $this[5], $this[6]->value );
		}
		else
		{
			$scope->use_( $this[5], $this[6]->value );
		}
		
		if( count( $this ) === 14 )
		{
			if( $this[8] )
			{
				$scope->declare_( $this[10], $this[11]->value );
			}
			else
			{
				$scope->use_( $this[10], $this[11]->value );
			}
		}
		
		$this[-1]->analyze_locals( $scope );
	}
	
	public function phpify( $out )
	{
		$out(
			$this[0],
			'(',
			$this[1],
			$this[2]
		);
		$out->comment( $this[3] );
		$out( $this->slice( 4, 7 ) );
		
		if( $this[7]->value === ':' )
		{
			$out( $this[7]->whitespace, '=>' );
			$out->comment( $this[8] );
			$out( $this->slice( 9, 12 ) );
		}
		
		$out(
			')',
			$this->slice( -2 )
		);
	}
}
