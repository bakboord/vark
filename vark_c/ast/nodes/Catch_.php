<?php

/*
Vark - compiles vark programs to PHP
Copyright (C) 2013 Alex Deleyn <alex@bakboord.be>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace vark\c\ast;
use vark\c\parse;

class Catch_ extends parse\Node
{
	private $tmp_var;
	
	public static function parse( $source )
	{
		$nodes = array();
		$nodes[] = $source->read( 'catch' );
		$nodes[] = util\parse_type( $source );
		if( $source->peek( 'as' ) )
		{
			$nodes[] = $source->read( 'as' );
			$nodes[] = ( $source->peek( 'var' ) ? $source->read() : NULL );
			$nodes[] = $source->read( '$' );
			$nodes[] = $source->read( 'identifier' );
		}
		else
		{
			$nodes[] = NULL;
			$nodes[] = NULL;
			$nodes[] = NULL;
			$nodes[] = NULL;
		}
		$nodes[] = $source->read( 'newline' );
		$nodes[] = util\parse_block( $source );
		return new self( $nodes );
	}
	
	public function analyze_locals( $scope )
	{
		if( !$this[2] )
		{
			$this->tmp_var = $scope->create_tmp_var( $scope );
		}
		
		if( $this[3] )
		{
			$scope->declare_( $this[4], $this[5]->value );
		}
		
		$this[7]->analyze_locals( $scope );
	}
	
	public function phpify( $out )
	{
		$out(
			$this[0],
			'(',
			$this[1]
		);
		
		if( $this[2] )
		{
			$out( $this[2]->whitespace );
			$out->comment( $this[3] );
			$out( $this[4], $this[5] );
		}
		else
		{
			$out(
				' $',
				$this->tmp_var
			);
		}
		
		$out(
			')',
			$this[6],
			$this[7]
		);
	}
}
