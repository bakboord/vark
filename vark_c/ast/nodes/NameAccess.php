<?php

/*
Vark - compiles vark programs to PHP
Copyright (C) 2013 Alex Deleyn <alex@bakboord.be>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace vark\c\ast;
use vark\c\parse;

class NameAccess extends parse\Node
{
	public function phpify( $out )
	{
		$out(
			$this[0],
			$this[1]->whitespace
		);
		if( $this[0] instanceof Identifier )
		{
			if( preg_match( '/(^|\\.)_*[A-Z][^\\.]*$/', $this[0][0]->value ) || in_array( $this[0][0]->value, array( 'self', 'parent', 'static' ) ) )
			{
				$out( '::' );
			}
			else
			{
				$out( '\\' );
			}
		}
		else if( $this[0] instanceof NameAccess )
		{
			if( preg_match( '/^_*[A-Z]/', $this[0][2]->value ) )
			{
				$out( '::' );
			}
			else
			{
				$out( '\\' );
			}
		}
		else
		{
			$out( '->' );
		}
		
		$out( $this[2] );
	}
	
	public function phpify_dynamic( $out )
	{
		if( $this[0] instanceof Identifier )
		{
			$left = $this[0][0];
			
			if( in_array( $left->value, array( 'self', 'parent', 'static' ) ) )
			{
				$out( 'array(' );
				
				if( $left->value === 'self' )
				{
					$out( '__CLASS__' );
				}
				elseif( $left->value === 'parent' )
				{
					$out( 'get_parent_class()' );
				}
				else
				{
					$out( 'get_called_class()' );
				}
				
				$out(
					$this[1]->whitespace,
					',',
					$this[2]->whitespace,
					'\'', $this[2]->value, '\'',
					')'
				);
			}
			elseif( preg_match( '/^_*[A-Z]/', $left->value ) )
			{
				$out(
					$this[0]->whitespace, $this[1]->whitespace, $this[2]->whitespace,
					'\'', $this[0]->value, '::', $this[2]->value, '\''
				);
			}
			else
			{
				$out(
					$this[0]->whitespace, $this[1]->whitespace, $this[2]->whitespace,
					'\'', $this[0]->value, '\\\\', $this[2]->value, '\''
				);
			}
		}
		elseif( $this[0] instanceof NameAccess )
		{
			TODO;
		}
		else
		{
			$out(
				'array(',
				$this[0],
				$this[1]->whitespace,
				',',
				'\'', $this[2]->value, '\'',
				')'
			);
		}
	}
}
