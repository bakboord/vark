<?php

/*
Vark - compiles vark programs to PHP
Copyright (C) 2013 Alex Deleyn <alex@bakboord.be>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace vark\c\ast;
use vark\c\parse;

class Lambda extends parse\Node
{
	private $scope;
	
	public static function parse( $source )
	{
		$nodes = array();
		$nodes[] = $source->read( 'lambda' );
		
		$arguments = array();
		if( $source->peek()->type !== ':' )
		{
			for(;;)
			{
				$a = array();
				$a[] = $source->read( '$' );
				$a[] = $source->read( 'identifier' );
				$arguments[] = new parse\Node( $a ); // @TODO get rid of anoninodes
				
				if( $source->peek()->type === ',' )
				{
					$arguments[] = $source->read();
				}
				else
				{
					break;
				}
			}
		}
		$nodes[] = new parse\Node( $arguments );
		
		$nodes[] = $source->read( ':' );
		$nodes[] = util\parse_expression( $source );
		return new self( $nodes );
	}
	
	public function analyze_locals( $scope )
	{
		$this->scope = $scope->spawn_closure();
		foreach( $this[1] as $i => $a )
		{
			if( $i % 2 === 0 )
			{
				$this->scope->declare_( $a[0], $a[1]->value );
			}
		}
		$this[-1]->analyze_locals( $this->scope );
	}
	
	public function phpify( $out )
	{
		$inherited = $this->scope->inherited;
		$this_hack = in_array( 'this', $inherited );
		
		if( $this_hack )
		{
			$out( 'i(' );
			$this_tmp = $this->scope->create_tmp_var();
			$inherited[ array_search( 'this', $inherited ) ] = $this_tmp;
		}
		
		$out(
			$this[0]->whitespace,
			'function(',
			$this[1],
			$this[2]->whitespace,
			')'
		);
		
		if( $this->scope->inherited )
		{
			$out(
				'use(',
				implode( ', ',
					array_map( function( $v ) { return '&$' . $v; }, $inherited )
				),
				')'
			);
		}
		
		$out(
			'{'
		);
		
		if( $this_hack )
		{
			$out( '${\'this\'.\'\'}=$' . $this_tmp . ';' );
		}
		
		$out(
			'return ',
			$this[3],
			';}'
		);
		
		if( $this_hack )
		{
			$out( ',$' . $this_tmp . '=$this)' );
		}
	}
}
