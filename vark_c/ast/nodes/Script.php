<?php

/*
Vark - compiles vark programs to PHP
Copyright (C) 2013 Alex Deleyn <alex@bakboord.be>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace vark\c\ast;
use vark\c\parse;

class Script extends parse\Node
{
	public static $dev_auto_compile;
	
	public static function parse( $source )
	{
		$statements = array();
		while( !$source->peek( 'eof' ) )
		{
			$statements[] = util\parse_statement( $source );
		}
		return new self( $statements );
	}
	
	public function phpify( $out )
	{
		$out( '<?php ' );
		$push = array();
		
		$i = 0;
		foreach( $this as $i => $n )
		{
			if( $n instanceof Namespace_ )
			{
				$out( $n );
			}
			else if( $n instanceof Nop || $n instanceof RuntimeLint )
			{
				$push[] = $n;
			}
			else
			{
				break;
			}
		}
		
		foreach( $push as $n )
		{
			$out( $n );
		}
		
		$runtime_base = realpath( __DIR__ . '/../../../runtime/' ) . '/';
		$out( 'include_once ' . var_export( $runtime_base . 'default.inc.php', TRUE ) . ';' );
		
		if( self::$dev_auto_compile )
		{
			$out( 'include_once ' . var_export( $runtime_base . 'dev_auto_compile.inc.php', TRUE ) . ';' );
			$out( 'if( !\vark\r\DevAutoCompile::is_up_to_date( __FILE__ ) ) { \vark\r\DevAutoCompile::recompile( __FILE__ ); return include __FILE__; }' );
			$out( '{' );
		}
		
		$out( $this->slice( $i ) );
		
		if( self::$dev_auto_compile )
		{
			$out( '}' );
		}
	}
}
