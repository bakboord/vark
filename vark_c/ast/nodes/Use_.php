<?php

/*
Vark - compiles vark programs to PHP
Copyright (C) 2013 Alex Deleyn <alex@bakboord.be>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace vark\c\ast;
use vark\c\parse;

class Use_ extends parse\Node
{
	public static function parse( $source )
	{
		$nodes = array();
		$nodes[] = $source->read( 'use' );
		$nodes[] = Name::parse( $source );
		// @TODO as ...
		$nodes[] = $source->read( 'newline' );
		return new self( $nodes );
	}
	
	public function get_aliases()
	{
		$aliases = array();
		
		if( count( $this ) > 3 )
		{
			TODO;
		}
		else
		{
			$aliases[ $this[1][-1]->value ] = '.' . implode( array_map( function( $x ) { return $x->value; }, $this[1]->value ) );
		}
		
		return $aliases;
	}
	
	public function phpify( $out )
	{
		$out->comment( $this[0], $this[1] );
		$out( $this[2] );
	}
}
