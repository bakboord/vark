<?php

/*
Vark - compiles vark programs to PHP
Copyright (C) 2013 Alex Deleyn <alex@bakboord.be>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace vark\c\ast;
use vark\c\parse;

class ArrayLiteral extends parse\Node
{
	public static function parse( $source )
	{
		$nodes = array();
		$nodes[] = $source->read( '[' );
		
		if( !$source->peek( ']' ) )
		{
			$a = array();
			$a[] = util\parse_expression( $source );
			if( $associative = $source->peek( ':' ) )
			{
				$a[] = $source->read();
				$a[] = util\parse_expression( $source );
			}
			$nodes[] = new ArrayLiteralItem( $a );
			
			if( $source->peek( ',' ) )
			{
				$nodes[] = $source->read();
			}
			else
			{
				goto end;
			}
			
			while( !$source->peek( ']' ) )
			{
				$a = array();
				$a[] = util\parse_expression( $source );
				if( $associative )
				{
					$a[] = $source->read( ':' );
					$a[] = util\parse_expression( $source );
				}
				$nodes[] = new ArrayLiteralItem( $a );
				
				if( $source->peek( ',' ) )
				{
					$nodes[] = $source->read();
				}
				else
				{
					goto end;
				}
			}
		}
		
		end:
		$nodes[] = $source->read( ']' );
		
		return new self( $nodes );
	}
	
	public function phpify( $out )
	{
		$out(
			$this[0]->whitespace,
			$this->parent instanceof Assignment && $this->index() === 0 ? 'list(' : 'array(',
			$this->slice( 1, -1 ),
			$this[-1]->whitespace,
			')'
		);
	}
}
