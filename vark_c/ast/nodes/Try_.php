<?php

/*
Vark - compiles vark programs to PHP
Copyright (C) 2013 Alex Deleyn <alex@bakboord.be>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace vark\c\ast;
use vark\c\parse;

class Try_ extends parse\Node
{
	private $scope;
	
	public static function parse( $source )
	{
		$nodes = array();
		$nodes[] = $source->read( 'try' );
		$nodes[] = $source->read( 'newline' );
		$nodes[] = util\parse_block( $source );
		
		while( $source->peek( 'catch' ) )
		{
			$nodes[] = Catch_::parse( $source );
		}
		
		return new self( $nodes );
	}
	
	public function analyze_locals( $scope )
	{
		if( $this[-1] )
		{
			$this->scope = $scope->spawn_sub();
			
			foreach( $this->slice( 2, -1 ) as $n )
			{
				$n->analyze_locals( $this->scope );
			}
			
			$this[-1]->analyze_locals( $scope );
		}
		else
		{
			parent::analyze_locals( $scope );
		}
	}
	
	public function phpify( $out )
	{
		parent::phpify( $out );
		
		if( count( $this ) === 3 )
		{
			$out( 'catch(\vark\r\Never$e){}' );
		}
	}
}
