<?php

/*
Vark - compiles vark programs to PHP
Copyright (C) 2013 Alex Deleyn <alex@bakboord.be>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace vark\c\ast;
use vark\c\parse;

class StringLiteral extends parse\Node
{
	public function evaluate()
	{
		$v = $this[0]->value;
		$n = strlen( $v ) - 2;
		$s = '';
		
		for( $i = 0 ; $i < $n ; $i += 1 )
		{
			$c = $v[ $i + 1 ];
			
			if( $c === '\\' )
			{
				$c2 = $v[ $i + 2 ];
				
				if( $c2 === 'n' )
				{
					$s .= "\n";
				}
				else if( $c2 === 't' )
				{
					$s .= "\t";
				}
				else if( $c2 === '"' )
				{
					$s .= '"';
				}
				else
				{
					$s .= $c . $c2;
				}
				
				$i += 1;
			}
			else
			{
				$s .= $c;
			}
		}
		
		return $s;
	}
	
	public function phpify( $out )
	{
		$v = $this[0]->value;
		$n = strlen( $v ) - 2;
		$s = '\'';
		$d = FALSE;
		$p = FALSE;
		
		for( $i = 0 ; $i < $n ; $i += 1 )
		{
			$c = $v[ $i + 1 ];
			
			if( $c === '\\' )
			{
				$c2 = $v[ $i + 2 ];
				
				if( $c2 === 'n' || $c2 === 't' )
				{
					if( !$d )
					{
						if( $i )
						{
							$s .= '\' . "';
							$p = TRUE;
						}
						else
						{
							$s = '"';
						}
						
						$d = TRUE;
					}
				}
				else if( $d )
				{
					$s .= '" . \'';
					$d = FALSE;
				}
				
				if( $c2 === '"' )
				{
					$s .= $c2;
				}
				else
				{
					$s .= '\\' . $c2;
				}
				
				$i += 1;
			}
			else
			{
				if( $d )
				{
					$s .= '" . \'';
					$d = FALSE;
					$p = TRUE;
				}
				
				if( $c === '\'' )
				{
					$s .= '\\';
				}
				
				$s .= $c;
			}
		}
		
		$s .= ( $d ? '"' : '\'' );
		
		if( $p )
		{
			$s = '(' . $s . ')';
		}
		
		$out(
			$this[0]->whitespace,
			$s
		);
	}
}
