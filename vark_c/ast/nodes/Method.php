<?php

/*
Vark - compiles vark programs to PHP
Copyright (C) 2013 Alex Deleyn <alex@bakboord.be>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace vark\c\ast;
use vark\c\parse;

class Method extends parse\Node
{
	public static function parse( $source )
	{
		$nodes = array();
		$keywords = array( $source->read_any( array( 'public', 'protected', 'private' ) ) );
		while( in_array( $source->peek()->type, array( 'static', 'abstract' ) ) )
		{
			$keywords[] = $source->read();
		}
		$nodes[] = new parse\Node( $keywords );
		$nodes[] = ( $source->peek( '&' ) ? $source->read() : NULL );
		$nodes[] = ( !$source->peek_ahead( 1, '(' ) ? util\parse_type( $source ) : NULL );
		$nodes[] = $source->read( 'identifier' );
		$nodes[] = $source->read( '(' );
		$nodes[] = new parse\Node( util\parse_arguments( $source ) );
		$nodes[] = $source->read( ')' );
		$nodes[] = $source->read( 'newline' );
		$nodes[] = ( $source->peek( '{' ) ? util\parse_block( $source ) : NULL );
		return new self( $nodes );
	}
	
	public function analyze_locals( $scope )
	{
		$my_scope = $scope->spawn_new();
		$my_scope->declare_( NULL, 'this' ); // @TODO if static
		foreach( $this[5] as $i => $a )
		{
			if( $i % 2 === 0 )
			{
				$my_scope->declare_( $a[2], $a[3]->value );
			}
		}
		$this[-1]->analyze_locals( $my_scope );
	}
	
	public function phpify( $out )
	{
		$out(
			$this[0],
			' function',
			$this[1]
		);
		$out->comment( $this[2] );
		$out(
			$this->slice( 3, -1 )
		);
		
		if( $this[-1] )
		{
			$out( $this[-1] );
		}
		else
		{
			$out( ';' );
		}
	}
}
