<?php

/*
Vark - compiles vark programs to PHP
Copyright (C) 2013 Alex Deleyn <alex@bakboord.be>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace vark\c\mod\names;
use vark\c\ast;

// @TODO rewrite

function fix( $ast )
{
	$namespace_nodes = $ast->find( function( $node )
	{
		return $node instanceof ast\Namespace_
			|| $node instanceof ast\Use_
			|| $node instanceof ast\Name
			|| $node instanceof ast\Identifier
		;
	} );
	
	$namespace = '';
	$aliased = array();
	
	foreach( $namespace_nodes as $node )
	{
		if( $node instanceof ast\Namespace_ )
		{
			$namespace = implode( array_map( function( $x )
			{
				return $x->value;
			}, $node[1]->value ) );
		}
		else if( $node instanceof ast\Use_ )
		{
			$aliased = array_merge( $aliased, $node->get_aliases() );
		}
		else if( $node instanceof ast\Name )
		{
			if( $node->parent instanceof ast\Namespace_ || $node->parent instanceof ast\Use_ )
			{
				// pass
			}
			else
			{
				_resolve( $namespace, $aliased, $node );
			}
		}
		else // identifier
		{
			if( $node->parent instanceof ast\ArrayLiteralItem && $node->index() === 0 )
			{
			}
			else
			{
				_resolve( $namespace, $aliased, $node );
			}
		}
	}
}

function _resolve( $namespace, $aliased, $node )
{
	static $magic_constants = array(
		'__FILE__',
		'__LINE__',
		'__CLASS__',
	);
	
	$v =& $node[0]->value;
	
	if( $v === 'php' )
	{
		assert( $node instanceof ast\Name || $node->parent instanceof ast\NameAccess );
		$v = '';
	}
	else if( $v === 'self' || $v === 'parent' || $v === 'static' )
	{
		// do nothing
	}
	else if( $node[0]->type === 'identifier' )
	{
		if( in_array( $v, $magic_constants ) )
		{
			// do nothing
		}
		else if( array_key_exists( $v, $aliased ) )
		{
			$v = $aliased[ $v ];
		}
		else if( $namespace )
		{
			$v = preg_replace( '/^(\s*)/', '\\1.', $namespace ) . '.' . $v;
		}
		else
		{
			$v = '.' . $v;
		}
	}
}
