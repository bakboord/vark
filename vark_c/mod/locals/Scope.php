<?php

/*
Vark - compiles vark programs to PHP
Copyright (C) 2013 Alex Deleyn <alex@bakboord.be>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace vark\c\mod\locals;

class Scope
{
	public $parent;
	public $declared = array( '_GET', '_POST', '_SERVER', '_COOKIE' );
	public $inherited = array();
	
	public function __construct( $parent = NULL )
	{
		$this->parent = $parent;
	}
	
	public function spawn_new()
	{
		return new self();
	}
	
	public function spawn_closure()
	{
		return new self( $this );
	}
	
	public function spawn_sub()
	{
		return new SubScope( $this );
	}
	
	public function declare_( $token, $k )
	{
		if( in_array( $k, $this->declared ) )
		{
			throw new Error( 'double declaration of local $' . $k, $token );
		}
		else if( in_array( $k, $this->inherited ) )
		{
			throw new Error( 'declaration of inherited local $' . $k, $token );
		}
		else
		{
			$this->declared[] = $k;
		}
	}
	
	public function use_( $token, $k )
	{
		if( !in_array( $k, $this->declared ) && !in_array( $k, $this->inherited ) )
		{
			if( $this->parent && $this->parent->has( $k ) )
			{
				$this->inherited[] = $k;
				$this->parent->use_( $token, $k );
			}
			else
			{
				throw new Error( 'use of undeclared local $' . $k, $token );
			}
		}
	}
	
	public function has( $k )
	{
		return in_array( $k, $this->declared ) || ( $this->parent && $this->parent->has( $k ) );
	}
	
	private $i = 0;
	public function create_tmp_var()
	{
		return 't' . $this->i++;
	}
}

class SubScope
{
	public $parent;
	public $used = array();
	
	public function __construct( $parent )
	{
		$this->parent = $parent;
	}
	
	public function spawn_new()
	{
	}
	
	public function spawn_closure()
	{
		return new Scope( $this );
	}
	
	public function spawn_sub()
	{
		return new SubScope( $this );
	}
	
	public function declare_( $token, $k )
	{
		$this->parent->declare_( $token, $k );
		$this->used[] = $k;
	}
	
	public function use_( $token, $k )
	{
		$this->parent->use_( $token, $k );
		$this->used[] = $k;
	}
	
	public function has( $k )
	{
		return $this->parent->has( $k );
	}
}
