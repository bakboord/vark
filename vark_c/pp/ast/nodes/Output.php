<?php

/*
Vark - compiles vark programs to PHP
Copyright (C) 2013 Alex Deleyn <alex@bakboord.be>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace vark\c\pp\ast;
use vark\c\parse;

class Output extends parse\Node
{
	public static function parse( $source )
	{
		$nodes = array();
		while( $source->peek()->type !== 'newline' )
		{
			if( $source->peek()->type === '#' )
			{
				$nodes[] = util\parse_inline_statement( $source );
			}
			else
			{
				$nodes[] = $source->read();
			}
		}
		$nodes[] = $source->read( 'newline' );
		return new self( $nodes );
	}
	
	public function preprocess( array &$tokens, $scope )
	{
		$my_tokens = array();
		foreach( $this as $t )
		{
			$t->preprocess( $my_tokens, $scope );
		}
		
		$scope->resolve_macros( $my_tokens );
		
		$tokens = array_merge( $tokens, $my_tokens );
	}
}
