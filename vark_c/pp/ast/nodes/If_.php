<?php

/*
Vark - compiles vark programs to PHP
Copyright (C) 2013 Alex Deleyn <alex@bakboord.be>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace vark\c\pp\ast;
use vark\c\parse;
use vark\c\ast;
use vark\c\lex;

class If_ extends parse\Node
{
	public static function parse( $source )
	{
		$nodes = array();
		$nodes[] = $source->read( '#' );
		$nodes[] = $source->read( 'if' );
		$nodes[] = ast\util\parse_expression( $source );
		$nodes[] = $source->read( 'newline' );
		
		$block = array();
		while( !$source->peek( '#' )
			|| (
				!( $source->peek_ahead( 1, '/' ) && $source->peek_ahead( 2, 'if' ) )
				&& !$source->peek_ahead( 1, 'elseif' )
				&& !$source->peek_ahead( 1, 'else' )
			)
		)
		{
			$block[] = util\parse_statement( $source );
		}
		$nodes[] = new parse\Node( $block );
		
		$elifs = array();
		while( $source->peek_ahead( 1, 'elseif' ) )
		{
			$elifs[] = ElseIf_::parse( $source );
		}
		$nodes[] = new parse\Node( $elifs );
		
		if( $source->peek_ahead( 1, 'else' ) )
		{
			$nodes[] = Else_::parse( $source );
		}
		 
		$nodes[] = $source->read( '#' );
		$nodes[] = $source->read( '/' );
		$nodes[] = $source->read( 'if' );
		$nodes[] = $source->read( 'newline' );
		return new self( $nodes );
	}
	
	public function preprocess( array &$tokens, $scope )
	{
		$branched = FALSE;
		
		if( $this[2]->evaluate( $scope ) )
		{
			$branched = TRUE;
			$tokens[] = lex\Token::create_whitespace( $this->slice( 0, 4 ) );
			$this[4]->preprocess( $tokens, $scope );
		}
		else
		{
			$tokens[] = lex\Token::create_whitespace( $this->slice( 0, 5 ) );
		}
		
		foreach( $this[5] as $e )
		{
			if( !$branched && $e[2]->evaluate( $scope ) )
			{
				$branched = TRUE;
				$tokens[] = lex\Token::create_whitespace( $e->slice( 0, 4 ) );
				$e[4]->preprocess( $tokens, $scope );
			}
			else
			{
				$tokens[] = lex\Token::create_whitespace( $e );
			}
		}
		
		if( $this[6] )
		{
			if( !$branched )
			{
				$tokens[] = lex\Token::create_whitespace( $this[6]->slice( 0, 3 ) );
				$this[6][3]->preprocess( $tokens, $scope );
			}
			else
			{
				$tokens[] = lex\Token::create_whitespace( $this[6] );
			}
		}
		
		$tokens[] = lex\Token::create_whitespace( $this->slice( -4 ) );
	}
}
