<?php

/*
Vark - compiles vark programs to PHP
Copyright (C) 2013 Alex Deleyn <alex@bakboord.be>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace vark\c\pp\ast;
use vark\c\parse;
use vark\c\lex;

class Script extends parse\Node
{
	private $filename;
	
	public static function parse( $source )
	{
		$filename = $source->peek()->filename;
		$statements = array();
		while( !$source->peek( 'eof' ) )
		{
			$statements[] = util\parse_statement( $source );
		}
		
		$script = new self( $statements );
		$script->filename = $filename;
		return $script;
	}
	
	public function load_magic( array &$tokens, $scope )
	{
		$directories = array();
		$d = dirname( realpath( $this->filename ) );
		do
		{
			array_unshift( $directories, $d );
			$d = dirname( $d );
		}
		while( $d !== $directories[0] );
		
		foreach( $directories as $d )
		{
			$filename = $d . '/__magic.vark';
			if( file_exists( $filename ) )
			{
				$magic_tokens = lex\Tokenizer::tokenize( $filename, file_get_contents( $filename ) );
				$magic_tokens = lex\Tokenizer::fix( $magic_tokens ); // merges whitespace tokens into significant ones
				
				// preprocessing
				$pp_ast = Script::parse( new parse\TokenScanner( $magic_tokens ) );
				$preprocessed_magic_tokens = array();
				$pp_ast->preprocess( $preprocessed_magic_tokens, $scope );
				$preprocessed_magic_tokens = lex\Tokenizer::fix( $preprocessed_magic_tokens ); // the preprocessor comments out some code
				
				foreach( $preprocessed_magic_tokens as $t )
				{
					if( $t->type === 'newline' )
					{
						$t->value = '';
					}
				}
				
				array_pop( $preprocessed_magic_tokens ); // pop eof
				
				$tokens = array_merge( $tokens, $preprocessed_magic_tokens );
			}
		}
	}
}
