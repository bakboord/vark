<?php

/*
Vark - compiles vark programs to PHP
Copyright (C) 2013 Alex Deleyn <alex@bakboord.be>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace vark\c\pp\ast;
use vark\c\parse;
use vark\c\ast;
use vark\c\lex;

class Include_ extends parse\Node
{
	public static function parse( $source )
	{
		$nodes = array();
		$nodes[] = $source->read( '#' );
		$nodes[] = $source->read( 'include' );
		$nodes[] = ast\util\parse_expression( $source );
		$nodes[] = $source->read( 'newline' );
		return new self( $nodes );
	}
	
	public function preprocess( array &$tokens, $scope )
	{
		$filename = $this[-2]->evaluate( $scope );
		
		$include_tokens = lex\Tokenizer::tokenize( $filename, file_get_contents( dirname( $this[0]->filename ) . '/' . $filename ) );
		$include_tokens = lex\Tokenizer::fix( $include_tokens ); // merges whitespace tokens into significant ones
		
		// preprocessing
		$pp_ast = Script::parse( new parse\TokenScanner( $include_tokens ) );
		$preprocessed_include_tokens = array();
		$pp_ast->preprocess( $preprocessed_include_tokens, $scope );
		$preprocessed_include_tokens = lex\Tokenizer::fix( $preprocessed_include_tokens ); // the preprocessor comments out some code
		
		array_pop( $preprocessed_include_tokens ); // pop eof
		
		$tokens = array_merge( $tokens, $preprocessed_include_tokens );
	}
}
