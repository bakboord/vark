<?php

/*
Vark - compiles vark programs to PHP
Copyright (C) 2013 Alex Deleyn <alex@bakboord.be>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace vark\c\pp\ast;
use vark\c\parse;

class Macro extends parse\Node
{
	public static function parse( $source )
	{
		$nodes = array();
		$nodes[] = $source->read( '#' );
		$nodes[] = $source->read( 'macro' );
		$nodes[] = ( $source->peek( '/' ) ? $source->read() : NULL );
		$nodes[] = $source->read( 'identifier' );
		
		$body = array();
		while( !$source->peek( 'newline' ) )
		{
			$body[] = $source->read();
		}
		$nodes[] = new parse\Node( $body );
		$nodes[] = $source->read();
		
		return new self( $nodes );
	}
	
	public function preprocess( array &$tokens, $scope )
	{
		$scope->define_macro( ( $this[2] ? '/' : '' ) . $this[3]->value, $this );
		
		foreach( $this[4] as $t )
		{
			if( $t->type === 'pp_newline' )
			{
				$t->super_type = $t->type = 'newline';
				$t->value = '/*' . $t->value . '*/';
			}
		}
	}
	
	public static function expand( $tokens )
	{
		$out = array();
		$stack = array();
		
		for( $i = 0 ; $i < count( $tokens ) ; $i += 1 )
		{
			$token = $tokens[ $i ];
			
			if( $token->type === 'macro_invocation' )
			{
				$macro = $token->value;
				$i += 1;
				$token = $tokens[ $i ];
				
				if( $token->type === '(' )
				{
					$stack[] = array( $macro, count( $out ) );
				}
				else
				{
					$out = array_merge( $out, $macro->_expand() );
				}
				
				$out[] = $token;
			}
			else if( $token->type === '(' )
			{
				$stack[] = NULL;
				$out[] = $token;
			}
			else if( $token->type === ')' )
			{
				list( $macro, $out_i ) = array_pop( $stack );
				
				if( $out_i !== NULL )
				{
					$parameter_tokens = array_slice( array_splice( $out, $out_i ), 1 );
					$parameters = array( $parameter_tokens, $parameter_tokens ); // @TODO
					
					$out = array_merge(
						$out,
						$macro->_expand( $parameters )
					);
				}
				else
				{
					$out[] = $token;
				}
			}
			else
			{
				$out[] = $token;
			}
		}
		
		return $out;
	}
	
	// @TODO inline
	private function _expand( $vars = array() )
	{
		$out = array();
		
		foreach( $this[4]->value as $t )
		{
			if( $t->type === 'macro_variable' )
			{
				$out = array_merge( $out, $vars[ intval( substr( $t->value, 1 ) ) ] );
			}
			else
			{
				$out[] = $t;
			}
		}
		
		return $out;
	}
}
