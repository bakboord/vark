<?php

/*
Vark - compiles vark programs to PHP
Copyright (C) 2013 Alex Deleyn <alex@bakboord.be>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace vark\c\pp\ast;
use vark\c\parse;
use vark\c\ast;
use vark\c\lex\token;

class Export extends parse\Node
{
	public static function parse( $source )
	{
		$nodes = array();
		$nodes[] = $source->read( '#' );
		$nodes[] = $source->read( 'export' );
		$nodes[] = ast\util\parse_expression( $source );
		$nodes[] = $source->read( 'newline' );
		return new self( $nodes );
	}
	
	public static function parse_inline( $source )
	{
		$nodes = array();
		$nodes[] = $source->read( '#' );
		$nodes[] = $source->read( '{' );
		$nodes[] = $source->read( 'export' );
		$nodes[] = ast\util\parse_expression( $source );
		$nodes[] = $source->read( '}' );
		return new self( $nodes );
	}
	
	public function preprocess( array &$tokens, $scope )
	{
		$v = $this[-2]->evaluate( $scope );
		
		if( $v === NULL )
		{
			$t = new Token( 'identifier', 'NULL', 'NULL' );
		}
		else if( is_bool( $v ) )
		{
			$v = $v ? 'TRUE' : 'FALSE';
			$t = new Token( 'identifier', $v, $v );
		}
		else
		{
			assert( FALSE ); // @TODO
		}
		
		$t->whitespace = $this[0]->whitespace;
		
		$tokens[] = $t;
	}
}
