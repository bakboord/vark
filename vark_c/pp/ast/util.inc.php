<?php

/*
Vark - compiles vark programs to PHP
Copyright (C) 2013 Alex Deleyn <alex@bakboord.be>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace vark\c\pp\ast\util;
use vark\c\pp\ast;

function parse_statement( $source )
{
	if( $source->peek( '#' ) && !$source->peek_ahead( 1, '{' ) )
	{
		switch( $source->peek_ahead( 1 )->type )
		{
			case 'include':
				return ast\Include_::parse( $source );
			case 'if':
				return ast\If_::parse( $source );
			case 'export':
				return ast\Export::parse( $source );
			case 'macro':
				return ast\Macro::parse( $source );
			case 'pragma':
				return ast\Pragma::parse( $source );
			default:
				return ast\ExpressionStatement::parse( $source );
		}
	}
	else
	{
		return ast\Output::parse( $source );
	}
}

function parse_inline_statement( $source )
{
	$t = $source->peek_ahead( 2 );
	switch( $t->type )
	{
		case 'export':
			return ast\Export::parse_inline( $source );
		default:
			TODO; // @TODO error
	}
}
