<?php

/*
Vark - compiles vark programs to PHP
Copyright (C) 2013 Alex Deleyn <alex@bakboord.be>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace vark\c\pp;
use vark\c\lex;

class Scope
{
	public $parent;
	public $vars = array();
	public $macros = array();
	
	public function __construct( $parent = NULL )
	{
		$this->parent = $parent;
	}
	
	public function spawn()
	{
		return new self( $this );
	}
	
	public function set( $k, $v )
	{
		$this->vars[ $k ] = $v;
	}
	
	public function get( $k )
	{
		if( array_key_exists( $k, $this->vars ) )
		{
			return $this->vars[ $k ];
		}
		else if( $this->parent )
		{
			return $this->parent->get( $k );
		}
		else
		{
			assert( FALSE ); // @TODO throw error
		}
	}
	
	public function &get_reference( $k )
	{
		return $this->vars[ $k ];
	}
	
	public function define_macro( $name, $macro )
	{
		$this->macros[ $name ] = $macro;
	}
	
	public function get_macro( $name )
	{
		return $this->macros[ $name ];
	}
	
	public function resolve_macros( $tokens )
	{
		foreach( $tokens as $t )
		{
			if( $t->type === 'macro_invocation' )
			{
				$t->value = $this->macros[ substr( $t->value, 1 ) ];
			}
		}
	}
}
