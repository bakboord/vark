<?php

/*
Vark - compiles vark programs to PHP
Copyright (C) 2013 Alex Deleyn <alex@bakboord.be>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace vark\c;

class Error extends \Exception
{
	public $message;
	public $token;
	
	public function __construct( $message, lex\Token $token )
	{
		$this->message = $message;
		$this->token = $token;
	}
	
	public function pretty_print( $stream, $indent = '' )
	{
		fwrite( $stream, $indent .
			util\terminal_color( '1;31', 'Oink! ' . static::NAME ) . "\n"
		);
		fwrite( $stream, $indent .
			'in '
			. util\terminal_color( '1', $this->token->filename )
			. ' at line '
			. util\terminal_color( '1', $this->token->line + 1 )
			. ':' . "\n" 
		);
		fwrite( $stream, $indent .
			util\terminal_color( '1;33', $this->message ) . "\n"
		);
		fwrite( $stream, "\n" );
		
		$lines = explode( "\n", $this->token->source );
		$line = $lines[ $this->token->line ];
		fwrite( $stream, $indent .
			util\terminal_color( '0;37', str_replace( "\t", '    ', $line ) ) . "\n"
		);
		fwrite( $stream, $indent .
			preg_replace( '/./', ' ', preg_replace( '/\\t/', '    ', substr( $line, 0, $this->token->offset ) ) ) . util\terminal_color( '1', '^' ) . "\n"
		);
	}
}
