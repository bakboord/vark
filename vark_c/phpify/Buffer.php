<?php

/*
Vark - compiles vark programs to PHP
Copyright (C) 2013 Alex Deleyn <alex@bakboord.be>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace vark\c\phpify;

class Buffer
{
	public $runtime_lint = FALSE;
	public $runtime_lint_included = FALSE;
	
	public function __construct()
	{
		ob_start();
	}
	
	public function __invoke()
	{
		foreach( func_get_args() as $arg )
		{
			if( is_array( $arg ) )
			{
				foreach( $arg as $a )
				{
					$this->__invoke( $a );
				}
			}
			else if( is_object( $arg ) )
			{
				$arg->phpify( $this );
			}
			else
			{
				echo $arg;
			}
		}
	}
	
	public function comment()
	{
		ob_start();
		foreach( func_get_args() as $a )
		{
			$this->__invoke( $a );
		}
		$b = ob_get_contents();
		ob_end_clean();
		
		echo \vark\c\util\comment( $b );
	}
	
	public function get_contents()
	{
		return ob_get_contents();
	}
	
	public function __destruct()
	{
		ob_end_clean();
	}
}
