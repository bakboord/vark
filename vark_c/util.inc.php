<?php

/*
Vark - compiles vark programs to PHP
Copyright (C) 2013 Alex Deleyn <alex@bakboord.be>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace vark\c\util;
use vark\c\lex\Token;

if( posix_isatty( STDOUT ) )
{
	function terminal_color( $color, $text )
	{
		return "\033[" . $color . 'm' . $text . "\033[0m";
	}
}
else
{
	function terminal_color( $color, $text )
	{
		return $text;
	}
}

function comment( $s )
{
	if( trim( $s ) )
	{
		return preg_replace( '/^(\s*)/', '\\1/*',
			preg_replace( '/(\s*)$/', '*/\\1',
				str_replace( '*/', '*\\/', $s )
			, 1 )
		);
	}
	else
	{
		return $s;
	}
}
