<?php

/*
Vark - compiles vark programs to PHP
Copyright (C) 2013 Alex Deleyn <alex@bakboord.be>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace vark\c\lex;

Tokenizer::register( '/[ \t]+/', 'whitespace' );
Tokenizer::register( '/\/\/.*?(?=\n)/', 'comment', 'whitespace' );
Tokenizer::register( '/\/(\*+).+?\1\//s', 'block_comment', 'whitespace' );

Tokenizer::register( '/\n(\s*\n)*/', 'newline' );

Tokenizer::register(
	'/ (?<![A-Za-z0-9_]) (
		abstract
		| and
		| as
		| assert
		| body
		| break
		| catch
		| class
		| const
		| continue
		| elseif
		| else
		| export
		| extends
		| foreach
		| for
		| function
		| if
		| implements
		| in
		| include
		| include_once
		| instanceof
		| interface
		| lambda
		| macro
		| namespace
		| new
		| or
		| pragma
		| print
		| public
		| protected
		| private
		| require
		| require_once
		| return
		| set
		| static
		| throw
		| try
		| var
		| while
		| use
		| TRUE
		| FALSE
		| NULL
	) (?![A-Za-z0-9_]) /x',
	NULL,
	'identifier'
);

/*Tokenizer::register(
	'/ (?<![A-Za-z]) (
		array
		| boolean
		| float
		| integer
		| string
		| void
	) (?![A-Za-z]) /x',
	'type',
	'identifier'
);*/

Tokenizer::register(
	'/
		\{ | \} | \( | \) | \[ | \]
		| ( \+ | - | \* | \/ | \.\.\. ) =? | =&
		| == | != | <= | >=
		| \.\.\. | ::
		| < | > | \? | , | : | = | \$ | \. | ! | & | ` | \#
	/x'
);

Tokenizer::register( '/ ( [1-9][0-9]* | 0 ) ( \. [0-9]+ )? /x', 'number' );
Tokenizer::register( '/ [A-Za-z_][A-Za-z0-9_]* /x', 'identifier' );
Tokenizer::register( '/ " ( [^"\\\\] | \\\\. )* " /x', 'string' );

Tokenizer::register( '/ @\\/?[A-Za-z_][A-Za-z0-9_]* /x', 'macro_invocation' );
Tokenizer::register( '/ @[0-9] /x', 'macro_variable' );
Tokenizer::register( '/\\\\n/', 'pp_newline' );
