<?php

/*
Vark - compiles vark programs to PHP
Copyright (C) 2013 Alex Deleyn <alex@bakboord.be>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace vark\c\lex;

class Token extends \vark\c\Node
{
	public $super_type;
	public $type;
	public $value;
	public $whitespace;
	
	public $filename;
	public $source;
	public $line;
	public $offset;
	
	public function __construct( $super_type, $type, $value )
	{
		$this->super_type = $super_type;
		$this->type = $type;
		$this->value = $value;
	}
	
	//
	// node stuff
	//
	public function preprocess( array &$tokens, $scope )
	{
		$tokens[] = $this;
	}
	
	public function phpify( $out )
	{
		$out(
			$this->whitespace . $this->value
		);
	}
	
	//
	// misc
	//
	public static function create_whitespace( $x )
	{
		return new self(
			'whitespace',
			'whitespace',
			\vark\c\util\comment( is_array( $x ) ? implode( $x ) : (string) $x )
		);
	}
	
	public function __toString()
	{
		return $this->whitespace . $this->value;
	}
}
