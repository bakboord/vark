<?php

/*
Vark - compiles vark programs to PHP
Copyright (C) 2013 Alex Deleyn <alex@bakboord.be>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace vark\c\lex;

abstract class Tokenizer
{
	private static $re = array();
	
	public static function register( $pattern, $type = NULL, $super_type = NULL )
	{
		self::$re[] = array( $pattern . 'A', $type, $super_type );
	}
	
	public static function tokenize( $filename, $source )
	{
		$i = 0;
		$n = strlen( $source );
		$tokens = array();
		$line = 0;
		$offset = 0;
		
		while( $i < $n )
		{
			foreach( self::$re as $r )
			{
				if( preg_match( $r[0], $source, $m, 0, $i ) )
				{
					$m = $m[0];
					
					$t = new Token( $r[2] ?: $r[1], $r[1] ?: $m, $m );
					$t->filename = $filename;
					$t->source = $source;
					$t->line = $line;
					$t->offset = $offset;
					$tokens[] = $t;
					
					if( strpos( $m, "\n" ) !== FALSE )
					{
						$line += substr_count( $m, "\n" );
						$offset = strlen( $m ) - strrpos( $m, "\n" ) - 1;
					}
					else
					{
						$offset += strlen( $m );
					}
					
					$i += strlen( $m );
					continue 2;
				}
			}
			
			$error_token = new Token( NULL, NULL, NULL );
			$error_token->filename = $filename;
			$error_token->source = $source;
			$error_token->line = $line;
			$error_token->offset = $offset;
			throw new Error( 'unexpected input', $error_token );
		}
		
		return $tokens;
	}
	
	public static function fix( $tokens )
	{
		$tokens = self::fix_comments( $tokens );
		$tokens = self::fix_whitespace( $tokens );
		$tokens = self::fix_newlines( $tokens );
		$tokens[] = new Token( 'eof', 'eof', NULL );
		return $tokens;
	}
	
	private static function fix_comments( $tokens )
	{
		foreach( $tokens as $t )
		{
			if( $t->type === 'block_comment' )
			{
				$t->value = preg_replace( '/\*\/(?=.*\*\/)/s', '*\\/', $t->value );
			}
		}
		
		return $tokens;
	}
	
	private static function fix_whitespace( $tokens )
	{
		$fixed = array();
		$whitespace = '';
		
		foreach( $tokens as $token )
		{
			if( $token->super_type === 'whitespace' )
			{
				$whitespace .= $token->value;
			}
			else
			{
				$token->whitespace = $whitespace . $token->whitespace;
				$fixed[] = $token;
				$whitespace = '';
			}
		}
		
		if( $whitespace !== '' )
		{
			$token = new Token( 'newline', 'newline', NULL );
			$token->whitespace = $whitespace;
			$fixed[] = $token;
		}
		
		return $fixed;
	}
	
	private static function fix_newlines( $tokens )
	{
		$stack = array(
			array( NULL, TRUE ),
		);
		$fixed = array();
		
		$match = array( '(' => ')', '[' => ']', '{' => '}' );
		
		foreach( $tokens as $i => $token )
		{
			if( $token->type === 'newline' )
			{
				$end = end( $stack );
				if( !$end[1] ) // we should ignore newlines
				{
					// append to the next token, if there is one
					if( $i + 1 < count( $tokens ) )
					{
						$tokens[ $i + 1 ]->whitespace = $token->whitespace . $token->value . $tokens[ $i + 1 ]->whitespace;
						continue;
					}
					// if there is no next token, we leave things be - we'll be running into a parse error soon enough
				}
				else if( $i > 0 && $tokens[ $i - 1 ]->type === 'newline' ) // subsequent newlines
				{
					array_pop( $fixed );
					$token->whitespace = $tokens[ $i - 1 ]->whitespace . $tokens[ $i - 1 ]->value . $token->whitespace;
				}
			}
			else
			{
				if( $token->type === '(' || $token->type === '[' ) // these always cause insensitivity
				{
					$stack[] = array( $token->type, FALSE );
				}
				else if( $token->type === '{' )
				{
					$stack[] = array( $token->type, TRUE );
				}
				else if( $token->type === ')' || $token->type === ']' || $token->type === '}' )
				{
					if( $match[ $stack[ count( $stack ) - 1 ][0] ] === $token->type )
					{
						array_pop( $stack );
					}
				}
			}
			
			$fixed[] = $token;
		}
		
		return $fixed;
	}
}
