<?php

/*
Vark - compiles vark programs to PHP
Copyright (C) 2013 Alex Deleyn <alex@bakboord.be>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace vark\c\parse;

class Node extends \vark\c\Node implements \ArrayAccess, \Iterator, \Countable
{
	public function __construct( array $nodes )
	{
		$this->value = $nodes;
		
		foreach( $nodes as $n )
		{
			if( $n )
			{
				$n->parent = $this;
			}
		}
	}
	
	public function count() { return count( $this->value ); }
	
	public function offsetExists( $k )
	{
		if( $k < 0 )
		{
			return -1 * $k <= count( $this->value );
		}
		else
		{
			$k < count( $this->value );
		}
	}
	public function offsetGet( $k ) { return $this->value[ $k < 0 ? $k + count( $this->value ) : $k ]; }
	public function offsetSet( $k, $v ) { assert( FALSE ); }
	public function offsetUnset( $k ) { assert( FALSE ); }
	
	private $i = 0;
	public function current() { return $this->value[ $this->i ]; }
	public function key() { return $this->i; }
	public function next() { $this->i += 1; }
	public function rewind() { $this->i = 0; }
	public function valid() { return $this->i < count( $this->value ); }
	
	public function index_of( $x )
	{
		return array_search( $x, $this->value );
	}
	
	public function slice( $start, $end = NULL )
	{
		if( $end === NULL )
		{
			return array_slice( $this->value, $start );
		}
		else if( $end < 0 )
		{
			return array_slice( $this->value, $start, count( $this->value ) + $end - $start );
		}
		else
		{
			return array_slice( $this->value, $start, $end - $start );
		}
	}
	
	public function find( $f, &$found = array() )
	{
		foreach( $this->value as $n )
		{
			if( $f( $n ) )
			{
				$found[] = $n;
			}
			
			if( $n instanceof self )
			{
				$n->find( $f, $found );
			}
		}
		
		return $found;
	}
	
	public function __toString()
	{
		return implode( $this->value );
	}
	
	public function preprocess( array &$tokens, $scope )
	{
		foreach( $this as $n )
		{
			$n->preprocess( $tokens, $scope );
		}
	}
	
	public function analyze_locals( $scope )
	{
		foreach( $this as $n )
		{
			if( $n instanceof self )
			{
				$n->analyze_locals( $scope );
			}
		}
	}
	
	public function phpify( $out )
	{
		$out( $this->value );
	}
}
