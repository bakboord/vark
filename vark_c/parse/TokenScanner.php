<?php

/*
Vark - compiles vark programs to PHP
Copyright (C) 2013 Alex Deleyn <alex@bakboord.be>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace vark\c\parse;

class TokenScanner
{
	public $offset = 0;
	private $tokens;
	
	public function __construct( array $tokens )
	{
		$this->tokens = $tokens;
		foreach( $this->tokens as $t )
		{
			assert( $t instanceof \vark\c\lex\Token );
		}
	}
	
	public function read( $type = NULL )
	{
		$t = $this->tokens[ $this->offset ];
		$this->offset += 1;
		
		if( $type && $t->type !== $type && $t->super_type !== $type )
		{
			throw new Error( 'expected ' . $type . ', got ' . $t->type, $t );
		}
		else
		{
			return $t;
		}
	}
	
	public function read_any( array $types )
	{
		$t = $this->tokens[ $this->offset ];
		$this->offset += 1;
		
		if( !in_array( $t->type, $types ) && !in_array( $t->super_type, $types ) )
		{
			throw new Error( 'unexpected ' . $t->type, $t );
		}
		else
		{
			return $t;
		}
	}
	
	public function peek( $type = NULL )
	{
		return $this->peek_ahead( 0, $type );
	}
	
	public function peek_ahead( $d, $type = NULL )
	{
		$d += $this->offset;
		
		if( $type !== NULL )
		{
			if( $d >= count( $this->tokens ) )
			{
				return FALSE;
			}
			else
			{
				$t = $this->tokens[ $d ];
				return $t->type === $type || $t->super_type === $type;
			}
		}
		else
		{
			return $this->tokens[ $d ];
		}
	}
	
	public function peek_any( $types )
	{
		$t = $this->tokens[ $this->offset ];
		return in_array( $t->type, $types ) || in_array( $t->super_type, $types );
	}
}
