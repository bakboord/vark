#!/usr/bin/php
<?php

/*
Vark - compiles vark programs to PHP
Copyright (C) 2013 Alex Deleyn <alex@bakboord.be>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace vark\c;

const USAGE = 'usage: vark file [file ...] or vark -w [directory ...]';

// helps with dev
set_error_handler( function()
{
	throw new \Exception( json_encode( array_slice( func_get_args(), 1, 3 ) ) );
} );

//
// argument parsing
//
$watch = FALSE;
$files = array();

$options = TRUE;
foreach( array_slice( $argv, 1 ) as $a )
{
	if( $options )
	{
		if( $a === '-w' )
		{
			$watch = TRUE;
		}
		else if( $a === '--' )
		{
			$options = FALSE;
		}
		else if( strlen( $a ) && $a[0] === '-' )
		{
			fwrite( STDERR, util\terminal_color( '1;31', 'Oink! unrecognized option: ' . $a ) . "\n" );
			fwrite( STDERR, USAGE . "\n" );
			exit( 1 );
		}
		else
		{
			$options = FALSE;
			$files[] = $a;
		}
	}
	else
	{
		$files[] = $a;
	}
}

//
// includes
//
set_include_path( implode( PATH_SEPARATOR, array(
	__DIR__ . '/vark_c',
	__DIR__ . '/inc',
) ) );
include 'Error.php';
include 'Node.php';
include 'util.inc.php';
include 'lex/Token.php';
include 'lex/Error.php';
include 'lex/Tokenizer.php';
include 'lex/tokens.inc.php';
include 'parse/Node.php';
include 'parse/TokenScanner.php';
include 'parse/Error.php';
include 'pp/Scope.php';

include 'pp/ast/util.inc.php';
foreach( glob( __DIR__ . '/vark_c/pp/ast/nodes/*.php' ) as $f )
{
	include $f;
}

include 'ast/util.inc.php';
foreach( glob( __DIR__ . '/vark_c/ast/nodes/*.php' ) as $f )
{
	include $f;
}

include 'mod/locals/Error.php';
include 'mod/locals/Scope.php';
include 'mod/names.inc.php';
include 'phpify/Buffer.php';

//
// compile a single file
//
function compile( $filename, $indent_output = '' )
{
	$buffer = NULL;
	
	// reset static pragma
	ast\Script::$dev_auto_compile = FALSE;
	
	try
	{
		// lexing
		$tokens = lex\Tokenizer::tokenize( $filename, file_get_contents( $filename ) );
		$tokens = lex\Tokenizer::fix( $tokens ); // merges whitespace tokens into significant ones
		
		// preprocessing
		$pp_ast = pp\ast\Script::parse( new parse\TokenScanner( $tokens ) );
		$preprocessed_tokens = array();
		$pp_scope = new pp\Scope();
		$pp_ast->load_magic( $preprocessed_tokens, $pp_scope );
		$pp_ast->preprocess( $preprocessed_tokens, $pp_scope );
		$preprocessed_tokens = pp\ast\Macro::expand( $preprocessed_tokens );
		$preprocessed_tokens = lex\Tokenizer::fix( $preprocessed_tokens ); // the preprocessor comments out some code
		
		// parse
		$ast = ast\Script::parse( new parse\TokenScanner( $preprocessed_tokens ) );
		
		// analyze var statements and scoping, closures need this to generate code
		$ast->analyze_locals( new mod\locals\Scope() );
		// fix the name resolution for functions
		mod\names\fix( $ast );
		
		// output
		$buffer = new phpify\Buffer();
		$ast->phpify( $buffer );
		file_put_contents( substr( $filename, 0, -4 ) . 'php', $buffer->get_contents() );
		return 0;
	}
	catch( \vark\c\Error $e )
	{
		if( $buffer )
		{
			$buffer->__destruct();
		}
		$e->pretty_print( STDERR, $indent_output );
		return 1;
	}
}

//
// invocation logic
//

if( !$watch )
{
	if( !$files )
	{
		fwrite( STDERR, USAGE . "\n" );
		exit( 1 );
	}
	
	function find( $directory )
	{
		$files = array();
		
		foreach( scandir( $directory ) as $f )
		{
			if( $f === '.' || $f === '..' ) continue;
			
			$f = $directory . '/' . $f;
			if( is_dir( $f ) )
			{
				$files = array_merge( $files, find( $f ) );
			}
			else if( substr( $f, strlen( $f ) - 5 ) === '.vark' )
			{
				$files[] = $f;
			}
		}
		
		return $files;
	}
	
	$r = 0;
	
	foreach( $files as $f )
	{
		if( is_dir( $f ) )
		{
			foreach( find( $f ) as $f )
			{
				$r += compile( $f );
			}
		}
		else
		{
			$r += compile( $f );
		}
	}
	
	exit( $r );
}
else
{
	echo 'vark is keeping an eye out for changes...' . "\n";
	
	function is_vark( $filename )
	{
		return substr( $filename, strlen( $filename ) - 5 ) === '.vark';
	}
	
	if( function_exists( 'inotify_init' ) )
	{
		$inotify = inotify_init();
		$inotify_descriptors = array();
		
		function watch_directory( $directory )
		{
			global $inotify, $inotify_descriptors;
			
			if( substr( $directory, -1 ) !== '/' ) $directory .= '/';
			
			$id = inotify_add_watch( $inotify, $directory, IN_CREATE | IN_MOVED_TO | IN_MOVED_FROM | IN_DELETE );
			$inotify_descriptors[ $id ] = $directory;
			
			foreach( scandir( $directory ) as $f )
			{
				if( $f === '.' || $f === '..' ) continue;
				
				$f = $directory . $f;
				if( is_dir( $f ) )
				{
					watch_directory( $f );
				}
				elseif( is_vark( $f ) )
				{
					watch_file( $f );
				}
			}
		}
		
		function watch_file( $filename )
		{
			global $inotify, $inotify_descriptors;
			
			$id = inotify_add_watch( $inotify, $filename, IN_MODIFY );
			$inotify_descriptors[ $id ] = $filename;
		}
		
		foreach( $files as $f )
		{
			if( is_dir( $f ) )
			{
				watch_directory( $f );
			}
			else
			{
				watch_file( $f );
			}
		}
		
		if( !$files )
		{
			watch_directory( $inotify, '' );
		}
		
		for(;;)
		{
			$events = array();
			do // merge closely following events
			{
				echo "loop" . inotify_queue_len( $inotify ) . ':' . count( $events ) . "\n";
				$events = array_merge( $events, inotify_read( $inotify ) );
				usleep( 0.2e6 );
			}
			while( inotify_queue_len( $inotify ) );
			echo "looped\n";
			
			$changes = array();
			
			foreach( $events as $e )
			{
				$name = $inotify_descriptors[ $e[ 'wd' ] ] . $e[ 'name' ];
				$mask = $e[ 'mask' ];
				
				if( $mask & IN_CREATE )
				{
					if( $mask & IN_ISDIR )
					{
						watch_directory( $name );
					}
					elseif( is_vark( $name ) )
					{
						watch_file( $name );
					}
				}
				elseif( is_vark( $name ) )
				{
					continue;
				}
				elseif( $mask & IN_MOVED_TO )
				{
					$changes[ $name ] = TRUE;
					watch_file( $name );
				}
				elseif( $mask & IN_MODIFY )
				{
					$changes[ $name ] = TRUE;
				}
				elseif( $mask & IN_DELETE || $mask & IN_MOVED_FROM )
				{
					$changes[ $name ] = FALSE;
				}
			}
			
			foreach( $changes as $f => $e )
			{
				if( $e )
				{
					echo '>> ' . $f . "\n";
					compile( $f );
				}
				else
				{
					echo '-- ' . $f . "\n";
					$f = substr( $f, 0, -2 ) . 'php';
					if( file_exists( $f ) )
					{
						unlink( $f );
					}
				}
			}
		}
	}
	else
	{
		define( 'TICK_BASE', 0.20e6 );
		define( 'TICK_SLOW', 0.01e6 );
		define( 'TICK_MAX', 3e6 );
		define( 'POLL_DIRECTORIES_EVERY', 4 );
		
		$watch_directories = array();
		$watch_files = array();
		
		function watch_directory( $directory )
		{
			global $watch_directories;
			
			if( substr( $directory, -1 ) !== '/' ) $directory .= '/';
			
			$watch_directories[ $directory ] = poll_directory( $directory );
			
			foreach( scandir( $directory ) as $f )
			{
				if( $f === '.' || $f === '..' ) continue;
				
				$f = $directory . $f;
				if( is_dir( $f ) )
				{
					watch_directory( $f );
				}
				elseif( is_vark( $f ) )
				{
					watch_file( $f );
				}
			}
		}
		
		function watch_file( $filename )
		{
			global $watch_files;
			
			if( !isset( $watch_files[ $filename ] ) )
			{
				$watch_files[ $filename ] = filemtime( $filename );
			}
		}
		
		function poll_directory( $directory )
		{
			$entries = array();
			foreach( scandir( $directory ) as $f )
			{
				if( $f === '.' || $f === '..' ) continue;
				
				$f = $directory . $f;
				if( is_vark( $f ) || is_dir( $f ) )
				{
					$entries[] = $f;
				}
			}
			return $entries;
		}
		
		foreach( $files as $f )
		{
			if( is_dir( $f ) )
			{
				watch_directory( $f );
			}
			else
			{
				watch_file( $f );
			}
		}
		
		if( !$files )
		{
			watch_directory( '.' );
		}
		
		$tick = TICK_BASE;
		
		for( $i = 0 ;; $i += 1 )
		{
			usleep( $tick );
			$tick = min( TICK_MAX, $tick + TICK_SLOW );
			
			$events = array();
			
			if( $i % POLL_DIRECTORIES_EVERY == 0 )
			foreach( $watch_directories as $directory => &$entries )
			{
				$new_entries = poll_directory( $directory );
				
				foreach( array_diff( $new_entries, $entries ) as $f )
				{
					$events[ $f ] = TRUE;
					watch_file( $f );
				}
				
				foreach( array_diff( $entries, $new_entries ) as $f )
				{
					$events[ $f ] = FALSE;
					unset( $watch_files[ $f ] );
				}
				
				$entries = $new_entries;
			}
			
			foreach( $watch_files as $file => &$mtime )
			{
				$new_mtime = filemtime( $file );
				
				if( $new_mtime > $mtime )
				{
					$events[ $file ] = TRUE;
					$mtime = $new_mtime;
				}
			}
			
			if( $events ) $tick = TICK_BASE;
			
			foreach( $events as $f => $e )
			{
				if( $e )
				{
					echo util\terminal_color( '1;32', '>> ' . $f ) . "\n";
					compile( $f, "\t" );
				}
				else
				{
					echo util\terminal_color( '1;31', '-- ' . $f ) . "\n";
					$f = substr( $f, 0, -2 ) . 'php';
					if( file_exists( $f ) )
					{
						unlink( $f );
					}
				}
			}
		}
	}
}
